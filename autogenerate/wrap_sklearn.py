# To wrap sklearn primitives in the TA1 API
import argparse
import os, sys, pip
import simplejson
from autogenerate import autogen_supervised, AutogenUtil

current_dir = os.path.abspath(os.path.join(os.path.realpath(__file__), os.pardir))


def generate_primitive_json(primitive_name, indent=4):
    from d3m import index
    all_primitives = index.search(primitive_path_prefix=primitive_name)

    primitive = all_primitives[0] if len(all_primitives) > 0 else None
    if primitive is None:
        print("Primitive not found: %(primitive_name)s", {'primitive_name': primitive_name})
        sys.exit(1)
    primitive = index.get_primitive(primitive)
    return primitive.metadata.to_json_structure()  # type: ignore


def create_module_dir(config: dict):
    package_name = config.get("package_name", "sklearn_wrap")
    output_dir = os.path.join(current_dir, "d3m_{}".format(package_name), package_name)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    with open(os.path.join(output_dir, '__init__.py'), 'w') as init_file:
        init_file.write('__author__ = \'{author}\'\n'.format(author=config['author']))
        init_file.write('__version__ = \'{version}\'\n'.format(version=config['version']))
    return output_dir


def generate_setup(config: dict):
    template_name = "setup.template"
    package_name = config.get("package_name", "sklearn_wrap")
    output_dir = os.path.join(current_dir, "d3m_{}".format(package_name))

    wrapper = AutogenUtil.TemplateUtil()
    wrapper.load_env()
    wrapper.load_template(template_name)
    setup_str = wrapper.render(package_name=package_name,
                               classes=config.get('classes'))
    with open(os.path.join(output_dir, "setup.py"), 'w') as fw:
        fw.write(setup_str)
    return output_dir


def generate_test(class_name, template_name="supervised.test.template"):
    output_dir = os.path.join(os.path.dirname(__file__), "..", "tests")

    wrapper = AutogenUtil.TemplateUtil()
    wrapper.load_env()
    wrapper.load_template(template_name)
    setup_str = wrapper.render(class_name=class_name)
    with open(os.path.join(output_dir, "test_{}.py".format(class_name)), 'w') as fw:
        fw.write(setup_str)


def main(args):
    config = simplejson.load(open(args.module_config, 'r'))
    output_dir = create_module_dir(config)
    version = config.get("version", "0.1.0")
    classes = []
    if args.overlays is not None:
        for overlay in args.overlays:
            overlay = simplejson.load(open(overlay, 'r'))
            for file in os.listdir(args.dir):
                class_name = autogen_supervised.generate_code(template_name=overlay.get('template'),
                                                              primitive_file=os.path.join(args.dir, file),
                                                              output_dir=output_dir,
                                                              overlay=overlay,
                                                              primitive_family=overlay.get("primitive_family"),
                                                              version=version)
                if class_name:
                    classes.append(class_name)
                    # generate_test(class_name, template_name=overlay.get("test_template"))
        classes = list(filter(lambda x: x is not None, classes))
        config.update({'classes': classes})
        generate_setup(config)

    if (args.primitives_repo):
        # Install new generated repo
        dist_path = args.dist_path
        if args.dist_path is None:
            dist_path = os.path.dirname(output_dir)
        pip.main(["install", "--force-reinstall", "-e", dist_path])
        for klass in classes:
            python_path = "{}.{}".format("d3m.primitives.sklearn_wrap", klass)
            print("Generating JSON for {}".format(python_path))
            primitive_json = generate_primitive_json(python_path)
            interfaces_version = primitive_json.get('primitive_code').get('interfaces_version')
            author = primitive_json.get('source').get('name')
            version = primitive_json.get('version')
            output = os.path.join(args.primitives_repo, "v{}".format(interfaces_version), author, "{}.{}".format("d3m.primitives.sklearn_wrap", klass), version, 'primitive.json')
            if not os.path.exists(os.path.dirname(output)):
                os.makedirs(os.path.dirname(output))
            with open(output, 'w') as fw:
                fw.write(simplejson.dumps(primitive_json, indent=4))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--dir", help="Directory containing json parsed from the code", required=True)
    parser.add_argument("--overlays", help="Overlay json to provide/override any additional metadata", nargs='+')
    parser.add_argument("--module_config", help="Config for generating the wrapped sklearn modules", required=True)
    parser.add_argument("--primitives_repo", help="Path to the primitives repo dir to place the annotations")
    parser.add_argument("--dist_path", help="Path to where the generated code is stored", required=False)
    args = parser.parse_args()

    if not os.path.exists(args.dir):
        print("{} does not exist".format(args.dir))
        exit(1)
    main(args)

