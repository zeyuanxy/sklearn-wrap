{
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Logistic Regression",
  "id": "fdece68a-8419-3bd9-a420-3acc698652bb",
  "category": "linear_model.logistic",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/logistic.py#L947",
  "parameters": [
    {
      "type": "str",
      "name": "penalty",
      "description": "Used to specify the norm used in the penalization. The \\'newton-cg\\', \\'sag\\' and \\'lbfgs\\' solvers support only l2 penalties. "
    },
    {
      "type": "bool",
      "name": "dual",
      "description": "Dual or primal formulation. Dual formulation is only implemented for l2 penalty with liblinear solver. Prefer dual=False when n_samples > n_features.  C : float, default: 1.0 Inverse of regularization strength; must be a positive float. Like in support vector machines, smaller values specify stronger regularization. "
    },
    {
      "type": "bool",
      "name": "fit_intercept",
      "description": "Specifies if a constant (a.k.a. bias or intercept) should be added to the decision function. "
    },
    {
      "type": "float",
      "name": "intercept_scaling",
      "description": "Useful only when the solver \\'liblinear\\' is used and self.fit_intercept is set to True. In this case, x becomes [x, self.intercept_scaling], i.e. a \"synthetic\" feature with constant value equal to intercept_scaling is appended to the instance vector. The intercept becomes ``intercept_scaling * synthetic_feature_weight``.  Note! the synthetic feature weight is subject to l1/l2 regularization as all other features. To lessen the effect of regularization on synthetic feature weight (and therefore on the intercept) intercept_scaling has to be increased. "
    },
    {
      "type": "dict",
      "name": "class_weight",
      "description": "Weights associated with classes in the form ``{class_label: weight}``. If not given, all classes are supposed to have weight one.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``.  Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified.  .. versionadded:: 0.17 *class_weight=\\'balanced\\'* instead of deprecated *class_weight=\\'auto\\'*. "
    },
    {
      "type": "int",
      "name": "max_iter",
      "description": "Useful only for the newton-cg, sag and lbfgs solvers. Maximum number of iterations taken for the solvers to converge. "
    },
    {
      "type": "int",
      "name": "random_state",
      "description": "The seed of the pseudo random number generator to use when shuffling the data. Used only in solvers \\'sag\\' and \\'liblinear\\'. "
    },
    {
      "type": "\\'newton-cg\\', \\'lbfgs\\', \\'liblinear\\', \\'sag\\'",
      "name": "solver",
      "description": "Algorithm to use in the optimization problem.  - For small datasets, \\'liblinear\\' is a good choice, whereas \\'sag\\' is faster for large ones. - For multiclass problems, only \\'newton-cg\\', \\'sag\\' and \\'lbfgs\\' handle multinomial loss; \\'liblinear\\' is limited to one-versus-rest schemes. - \\'newton-cg\\', \\'lbfgs\\' and \\'sag\\' only handle L2 penalty.  Note that \\'sag\\' fast convergence is only guaranteed on features with approximately the same scale. You can preprocess the data with a scaler from sklearn.preprocessing.  .. versionadded:: 0.17 Stochastic Average Gradient descent solver. "
    },
    {
      "type": "float",
      "name": "tol",
      "description": "Tolerance for stopping criteria. "
    },
    {
      "type": "str",
      "name": "multi_class",
      "description": "Multiclass option can be either \\'ovr\\' or \\'multinomial\\'. If the option chosen is \\'ovr\\', then a binary problem is fit for each label. Else the loss minimised is the multinomial loss fit across the entire probability distribution. Works only for the \\'newton-cg\\', \\'sag\\' and \\'lbfgs\\' solver.  .. versionadded:: 0.18 Stochastic Average Gradient descent solver for \\'multinomial\\' case. "
    },
    {
      "type": "int",
      "name": "verbose",
      "description": "For the liblinear and lbfgs solvers set verbose to any positive number for verbosity. "
    },
    {
      "type": "bool",
      "name": "warm_start",
      "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. Useless for liblinear solver.  .. versionadded:: 0.17 *warm_start* to support *lbfgs*, *newton-cg*, *sag* solvers. "
    },
    {
      "type": "int",
      "name": "n_jobs",
      "description": "Number of CPU cores used during the cross-validation loop. If given a value of -1, all cores are used. "
    }
  ],
  "tags": [
    "linear_model",
    "regression",
    "logistic"
  ],
  "common_name_unanalyzed": "Logistic Regression",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "description": "'Logistic Regression (aka logit, MaxEnt) classifier.\n\nIn the multiclass case, the training algorithm uses the one-vs-rest (OvR)\nscheme if the \\'multi_class\\' option is set to \\'ovr\\', and uses the cross-\nentropy loss if the \\'multi_class\\' option is set to \\'multinomial\\'.\n(Currently the \\'multinomial\\' option is supported only by the \\'lbfgs\\',\n\\'sag\\' and \\'newton-cg\\' solvers.)\n\nThis class implements regularized logistic regression using the\n\\'liblinear\\' library, \\'newton-cg\\', \\'sag\\' and \\'lbfgs\\' solvers. It can handle\nboth dense and sparse input. Use C-ordered arrays or CSR matrices\ncontaining 64-bit floats for optimal performance; any other input format\nwill be converted (and copied).\n\nThe \\'newton-cg\\', \\'sag\\', and \\'lbfgs\\' solvers support only L2 regularization\nwith primal formulation. The \\'liblinear\\' solver supports both L1 and L2\nregularization, with a dual formulation only for the L2 penalty.\n\nRead more in the :ref:`User Guide <logistic_regression>`.\n",
  "methods_available": [
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.decision_function",
      "returns": {
        "name": "array, shape=(n_samples,) if n_classes == 2 else (n_samples, n_classes)",
        "description": "Confidence scores per (sample, class) combination. In the binary case, confidence score for self.classes_[1] where >0 means this class would be predicted. '"
      },
      "description": "'Predict confidence scores for samples.\n\nThe confidence score for a sample is the signed distance of that\nsample to the hyperplane.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "decision_function"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.densify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to dense array format.\n\nConverts the ``coef_`` member (back) to a numpy.ndarray. This is the\ndefault format of ``coef_`` and is required for fitting, so calling\nthis method is only required on models that have previously been\nsparsified; otherwise, it is a no-op.\n",
      "parameters": [],
      "name": "densify"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      },
      "description": "'Fit the model according to the given training data.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Training vector, where n_samples is the number of samples and n_features is the number of features. "
        },
        {
          "shape": "n_samples,",
          "type": "array-like",
          "name": "y",
          "description": "Target vector relative to X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples,",
          "optional": "true",
          "name": "sample_weight",
          "description": "Array of weights that are assigned to individual samples. If not provided, then each sample is given unit weight.  .. versionadded:: 0.17 *sample_weight* support to LogisticRegression. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.fit_transform",
      "returns": {
        "shape": "n_samples, n_features_new",
        "type": "numpy",
        "name": "X_new",
        "description": "Transformed array.  '"
      },
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "numpy",
          "name": "X",
          "description": "Training set. "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "C",
        "description": "Predicted class label per sample. '"
      },
      "description": "'Predict class labels for samples in X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.predict_log_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array-like",
        "name": "T",
        "description": "Returns the log-probability of the sample for each class in the model, where classes are ordered as they are in ``self.classes_``. '"
      },
      "description": "'Log of probability estimates.\n\nThe returned estimates for all classes are ordered by the\nlabel of classes.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": ""
        }
      ],
      "name": "predict_log_proba"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.predict_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array-like",
        "name": "T",
        "description": "Returns the probability of the sample for each class in the model, where classes are ordered as they are in ``self.classes_``. '"
      },
      "description": "'Probability estimates.\n\nThe returned estimates for all classes are ordered by the\nlabel of classes.\n\nFor a multi_class problem, if multi_class is set to be \"multinomial\"\nthe softmax function is used to find the predicted probability of\neach class.\nElse use a one-vs-rest approach, i.e calculate the probability\nof each class assuming it to be positive using the logistic function.\nand normalize these values across all the classes.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": ""
        }
      ],
      "name": "predict_proba"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.sparsify",
      "returns": {
        "name": "self: estimator",
        "description": "'"
      },
      "description": "'Convert coefficient matrix to sparse format.\n\nConverts the ``coef_`` member to a scipy.sparse matrix, which for\nL1-regularized models can be much more memory- and storage-efficient\nthan the usual numpy.ndarray representation.\n\nThe ``intercept_`` member is not converted.\n\nNotes\n-----\nFor non-sparse models, i.e. when there are not many zeros in ``coef_``,\nthis may actually *increase* memory usage, so use this method with\ncare. A rule of thumb is that the number of zero elements, which can\nbe computed with ``(coef_ == 0).sum()``, must be more than 50% for this\nto provide significant benefits.\n\nAfter calling this method, further fitting with the partial_fit\nmethod (if any) will not work until you call densify.\n",
      "parameters": [],
      "name": "sparsify"
    },
    {
      "id": "sklearn.linear_model.logistic.LogisticRegression.transform",
      "returns": {
        "shape": "n_samples, n_selected_features",
        "type": "array",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      },
      "description": "'DEPRECATED: Support to use estimators as feature selectors will be removed in version 0.19. Use SelectFromModel instead.\n\nReduce X to its most important features.\n\nUses ``coef_`` or ``feature_importances_`` to determine the most\nimportant features.  For models with a ``coef_`` for each class, the\nabsolute sum over the classes is used.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. "
        },
        {
          "default": "None",
          "type": "string",
          "optional": "true",
          "name": "threshold",
          "description": "The threshold value to use for feature selection. Features whose importance is greater or equal are kept while the others are discarded. If \"median\" (resp. \"mean\"), then the threshold value is the median (resp. the mean) of the feature importances. A scaling factor (e.g., \"1.25*mean\") may also be used. If None and if available, the object attribute ``threshold`` is used. Otherwise, \"mean\" is used by default. "
        }
      ],
      "name": "transform"
    }
  ],
  "algorithm_type": [
    "regression"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling",
    "feature extraction"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/logistic.py#L947",
  "category_unanalyzed": "linear_model.logistic",
  "name": "sklearn.linear_model.logistic.LogisticRegression",
  "team": "jpl",
  "attributes": [
    {
      "shape": "n_classes, n_features",
      "type": "array",
      "name": "coef_",
      "description": "Coefficient of the features in the decision function. "
    },
    {
      "shape": "n_classes,",
      "type": "array",
      "name": "intercept_",
      "description": "Intercept (a.k.a. bias) added to the decision function. If `fit_intercept` is set to False, the intercept is set to zero. "
    },
    {
      "shape": "n_classes,",
      "type": "array",
      "name": "n_iter_",
      "description": "Actual number of iterations for all classes. If binary or multinomial, it returns only 1 element. For liblinear solver, only the maximum number of iteration across all classes is given.  See also -------- SGDClassifier : incrementally trained logistic regression (when given the parameter ``loss=\"log\"``). sklearn.svm.LinearSVC : learns SVM models using the same algorithm. "
    }
  ]
}
