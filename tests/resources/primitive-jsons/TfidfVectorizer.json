{
  "name": "sklearn.feature_extraction.text.TfidfVectorizer",
  "id": "97c5a251c925796ef2d9314a964e1d47",
  "common_name": "TfidfVectorizer",
  "is_class": true,
  "tags": [
    "feature_extraction",
    "text"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/feature_extraction/text.py#L1125",
  "parameters": [
    {
      "type": "string",
      "name": "input",
      "description": "If \\'filename\\', the sequence passed as an argument to fit is expected to be a list of filenames that need reading to fetch the raw content to analyze.  If \\'file\\', the sequence items must have a \\'read\\' method (file-like object) that is called to fetch the bytes in memory.  Otherwise the input is expected to be the sequence strings or bytes items are expected to be analyzed directly. "
    },
    {
      "type": "string",
      "name": "encoding",
      "description": "If bytes or files are given to analyze, this encoding is used to decode. "
    },
    {
      "type": "\\'strict\\', \\'ignore\\', \\'replace\\'",
      "name": "decode_error",
      "description": "Instruction on what to do if a byte sequence is given to analyze that contains characters not of the given `encoding`. By default, it is \\'strict\\', meaning that a UnicodeDecodeError will be raised. Other values are \\'ignore\\' and \\'replace\\'. "
    },
    {
      "type": "\\'ascii\\', \\'unicode\\', None",
      "name": "strip_accents",
      "description": "Remove accents during the preprocessing step. \\'ascii\\' is a fast method that only works on characters that have an direct ASCII mapping. \\'unicode\\' is a slightly slower method that works on any characters. None (default) does nothing. "
    },
    {
      "type": "string",
      "name": "analyzer",
      "description": "Whether the feature should be made of word or character n-grams.  If a callable is passed it is used to extract the sequence of features out of the raw, unprocessed input. "
    },
    {
      "type": "callable",
      "name": "preprocessor",
      "description": "Override the preprocessing (string transformation) stage while preserving the tokenizing and n-grams generation steps. "
    },
    {
      "type": "callable",
      "name": "tokenizer",
      "description": "Override the string tokenization step while preserving the preprocessing and n-grams generation steps. Only applies if ``analyzer == \\'word\\'``. "
    },
    {
      "type": "tuple",
      "name": "ngram_range",
      "description": "The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used. "
    },
    {
      "type": "string",
      "name": "stop_words",
      "description": "If a string, it is passed to _check_stop_list and the appropriate stop list is returned. \\'english\\' is currently the only supported string value.  If a list, that list is assumed to contain stop words, all of which will be removed from the resulting tokens. Only applies if ``analyzer == \\'word\\'``.  If None, no stop words will be used. max_df can be set to a value in the range [0.7, 1.0) to automatically detect and filter stop words based on intra corpus document frequency of terms. "
    },
    {
      "type": "boolean",
      "name": "lowercase",
      "description": "Convert all characters to lowercase before tokenizing. "
    },
    {
      "type": "string",
      "name": "token_pattern",
      "description": "Regular expression denoting what constitutes a \"token\", only used if ``analyzer == \\'word\\'``. The default regexp selects tokens of 2 or more alphanumeric characters (punctuation is completely ignored and always treated as a token separator). "
    },
    {
      "type": "float",
      "name": "max_df",
      "description": "When building the vocabulary ignore terms that have a document frequency strictly higher than the given threshold (corpus-specific stop words). If float, the parameter represents a proportion of documents, integer absolute counts. This parameter is ignored if vocabulary is not None. "
    },
    {
      "type": "float",
      "name": "min_df",
      "description": "When building the vocabulary ignore terms that have a document frequency strictly lower than the given threshold. This value is also called cut-off in the literature. If float, the parameter represents a proportion of documents, integer absolute counts. This parameter is ignored if vocabulary is not None. "
    },
    {
      "type": "int",
      "name": "max_features",
      "description": "If not None, build a vocabulary that only consider the top max_features ordered by term frequency across the corpus.  This parameter is ignored if vocabulary is not None. "
    },
    {
      "type": "",
      "optional": "true",
      "name": "vocabulary",
      "description": "Either a Mapping (e.g., a dict) where keys are terms and values are indices in the feature matrix, or an iterable over terms. If not given, a vocabulary is determined from the input documents. "
    },
    {
      "type": "boolean",
      "name": "binary",
      "description": "If True, all non-zero term counts are set to 1. This does not mean outputs will have only 0/1 values, only that the tf term in tf-idf is binary. (Set idf and normalization to False to get 0/1 outputs.) "
    },
    {
      "type": "type",
      "optional": "true",
      "name": "dtype",
      "description": "Type of the matrix returned by fit_transform() or transform(). "
    },
    {
      "type": "",
      "optional": "true",
      "name": "norm",
      "description": "Norm used to normalize term vectors. None for no normalization. "
    },
    {
      "type": "boolean",
      "name": "use_idf",
      "description": "Enable inverse-document-frequency reweighting. "
    },
    {
      "type": "boolean",
      "name": "smooth_idf",
      "description": "Smooth idf weights by adding one to document frequencies, as if an extra document was seen containing every term in the collection exactly once. Prevents zero divisions. "
    },
    {
      "type": "boolean",
      "name": "sublinear_tf",
      "description": "Apply sublinear tf scaling, i.e. replace tf with 1 + log(tf). "
    }
  ],
  "attributes": [
    {
      "type": "dict",
      "name": "vocabulary_",
      "description": "A mapping of terms to feature indices. "
    },
    {
      "type": "array",
      "shape": "n_features",
      "name": "idf_",
      "description": "The learned idf vector (global term weights) when ``use_idf`` is set to True, None otherwise. "
    },
    {
      "type": "set",
      "name": "stop_words_",
      "description": "Terms that were ignored because they either:  - occurred in too many documents (`max_df`) - occurred in too few documents (`min_df`) - were cut off by feature selection (`max_features`).  This is only available if no vocabulary was given.  See also -------- CountVectorizer Tokenize the documents and count the occurrences of token and return them as a sparse matrix  TfidfTransformer Apply Term Frequency Inverse Document Frequency normalization to a sparse matrix of occurrence counts. "
    }
  ],
  "description": "'Convert a collection of raw documents to a matrix of TF-IDF features.\n\nEquivalent to CountVectorizer followed by TfidfTransformer.\n\nRead more in the :ref:`User Guide <text_feature_extraction>`.\n",
  "methods_available": [
    {
      "name": "build_analyzer",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.build_analyzer",
      "parameters": [],
      "description": "'Return a callable that handles preprocessing and tokenization'"
    },
    {
      "name": "build_preprocessor",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.build_preprocessor",
      "parameters": [],
      "description": "'Return a function to preprocess the text before tokenization'"
    },
    {
      "name": "build_tokenizer",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.build_tokenizer",
      "parameters": [],
      "description": "'Return a function that splits a string into a sequence of tokens'"
    },
    {
      "name": "decode",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.decode",
      "parameters": [],
      "description": "'Decode the input into a string of unicode symbols\n\nThe decoding strategy depends on the vectorizer parameters.\n'"
    },
    {
      "name": "fit",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.fit",
      "parameters": [
        {
          "type": "iterable",
          "name": "raw_documents",
          "description": "an iterable which yields either str, unicode or file objects "
        }
      ],
      "description": "'Learn vocabulary and idf from training set.\n",
      "returns": {
        "type": "",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "fit_transform",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.fit_transform",
      "parameters": [
        {
          "type": "iterable",
          "name": "raw_documents",
          "description": "an iterable which yields either str, unicode or file objects "
        }
      ],
      "description": "'Learn vocabulary and idf, return term-document matrix.\n\nThis is equivalent to fit followed by transform, but more efficiently\nimplemented.\n",
      "returns": {
        "type": "sparse",
        "name": "X",
        "description": "Tf-idf-weighted document-term matrix. '"
      }
    },
    {
      "name": "get_feature_names",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.get_feature_names",
      "parameters": [],
      "description": "'Array mapping from feature integer indices to feature name'"
    },
    {
      "name": "get_params",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "get_stop_words",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.get_stop_words",
      "parameters": [],
      "description": "'Build or fetch the effective stop words list'"
    },
    {
      "name": "inverse_transform",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.inverse_transform",
      "parameters": [
        {
          "type": "array, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": ""
        }
      ],
      "description": "'Return terms per document with nonzero entries in X.\n",
      "returns": {
        "type": "list",
        "name": "X_inv",
        "description": "List of arrays of terms. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.feature_extraction.text.TfidfVectorizer.transform",
      "parameters": [
        {
          "type": "iterable",
          "name": "raw_documents",
          "description": "an iterable which yields either str, unicode or file objects "
        },
        {
          "type": "boolean",
          "name": "copy",
          "description": "Whether to copy X and operate on the copy or perform in-place operations. "
        }
      ],
      "description": "'Transform documents to document-term matrix.\n\nUses the vocabulary and document frequencies (df) learned by fit (or\nfit_transform).\n",
      "returns": {
        "type": "sparse",
        "name": "X",
        "description": "Tf-idf-weighted document-term matrix. '"
      }
    }
  ]
}