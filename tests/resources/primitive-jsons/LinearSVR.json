{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Linear SVR", 
  "id": "3f0ae7ab-dc9b-37fa-b410-bf4380d3370b", 
  "category": "svm.classes", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/svm/classes.py#L226", 
  "parameters": [
    {
      "default": "1.0", 
      "type": "float", 
      "optional": "true", 
      "name": "C", 
      "description": "Penalty parameter C of the error term. The penalty is a squared l2 penalty. The bigger this parameter, the less regularization is used. "
    }, 
    {
      "type": "string", 
      "name": "loss", 
      "description": "Specifies the loss function. \\'l1\\' is the epsilon-insensitive loss (standard SVR) while \\'l2\\' is the squared epsilon-insensitive loss. "
    }, 
    {
      "default": "0.1", 
      "type": "float", 
      "optional": "true", 
      "name": "epsilon", 
      "description": "Epsilon parameter in the epsilon-insensitive loss function. Note that the value of this parameter depends on the scale of the target variable y. If unsure, set ``epsilon=0``. "
    }, 
    {
      "type": "bool", 
      "name": "dual", 
      "description": "Select the algorithm to either solve the dual or primal optimization problem. Prefer dual=False when n_samples > n_features. "
    }, 
    {
      "default": "1e-4", 
      "type": "float", 
      "optional": "true", 
      "name": "tol", 
      "description": "Tolerance for stopping criteria. "
    }, 
    {
      "default": "True", 
      "type": "boolean", 
      "optional": "true", 
      "name": "fit_intercept", 
      "description": "Whether to calculate the intercept for this model. If set to false, no intercept will be used in calculations (i.e. data is expected to be already centered). "
    }, 
    {
      "default": "1", 
      "type": "float", 
      "optional": "true", 
      "name": "intercept_scaling", 
      "description": "When self.fit_intercept is True, instance vector x becomes [x, self.intercept_scaling], i.e. a \"synthetic\" feature with constant value equals to intercept_scaling is appended to the instance vector. The intercept becomes intercept_scaling * synthetic feature weight Note! the synthetic feature weight is subject to l1/l2 regularization as all other features. To lessen the effect of regularization on synthetic feature weight (and therefore on the intercept) intercept_scaling has to be increased. "
    }, 
    {
      "type": "int", 
      "name": "verbose", 
      "description": "Enable verbose output. Note that this setting takes advantage of a per-process runtime setting in liblinear that, if enabled, may not work properly in a multithreaded context. "
    }, 
    {
      "type": "int", 
      "name": "random_state", 
      "description": "The seed of the pseudo random number generator to use when shuffling the data. "
    }, 
    {
      "type": "int", 
      "name": "max_iter", 
      "description": "The maximum number of iterations to be run. "
    }
  ], 
  "tags": [
    "svm", 
    "classes"
  ], 
  "common_name_unanalyzed": "Linear SVR", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "'Linear Support Vector Regression.\n\nSimilar to SVR with parameter kernel=\\'linear\\', but implemented in terms of\nliblinear rather than libsvm, so it has more flexibility in the choice of\npenalties and loss functions and should scale better to large numbers of\nsamples.\n\nThis class supports both dense and sparse input.\n\nRead more in the :ref:`User Guide <svm_regression>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.svm.classes.LinearSVR.decision_function", 
      "returns": {
        "shape": "n_samples,", 
        "type": "array", 
        "name": "C", 
        "description": "Returns predicted values. '"
      }, 
      "description": "'DEPRECATED:  and will be removed in 0.19.\n\nDecision function of the linear model.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Samples. "
        }
      ], 
      "name": "decision_function"
    }, 
    {
      "id": "sklearn.svm.classes.LinearSVR.fit", 
      "returns": {
        "type": "object", 
        "name": "self", 
        "description": "Returns self. '"
      }, 
      "description": "'Fit the model according to the given training data.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Training vector, where n_samples in the number of samples and n_features is the number of features. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "Target vector relative to X "
        }, 
        {
          "type": "array-like", 
          "shape": "n_samples", 
          "optional": "true", 
          "name": "sample_weight", 
          "description": "Array of weights that are assigned to individual samples. If not provided, then each sample is given unit weight. "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.svm.classes.LinearSVR.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.svm.classes.LinearSVR.predict", 
      "returns": {
        "shape": "n_samples,", 
        "type": "array", 
        "name": "C", 
        "description": "Returns predicted values. '"
      }, 
      "description": "'Predict using the linear model\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Samples. "
        }
      ], 
      "name": "predict"
    }, 
    {
      "id": "sklearn.svm.classes.LinearSVR.score", 
      "returns": {
        "type": "float", 
        "name": "score", 
        "description": "R^2 of self.predict(X) wrt. y. '"
      }, 
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the regression\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the residual\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nBest possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Test samples. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "True values for X. "
        }, 
        {
          "type": "array-like", 
          "shape": "n_samples", 
          "optional": "true", 
          "name": "sample_weight", 
          "description": "Sample weights. "
        }
      ], 
      "name": "score"
    }, 
    {
      "id": "sklearn.svm.classes.LinearSVR.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }
  ], 
  "learning_type": [
    "supervised"
  ], 
  "task_type": [
    "modeling"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/svm/classes.py#L226", 
  "category_unanalyzed": "svm.classes", 
  "name": "sklearn.svm.classes.LinearSVR", 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_features", 
      "type": "array", 
      "name": "coef_", 
      "description": "Weights assigned to the features (coefficients in the primal problem). This is only available in the case of a linear kernel.  `coef_` is a readonly property derived from `raw_coef_` that follows the internal memory layout of liblinear. "
    }, 
    {
      "shape": "1", 
      "type": "array", 
      "name": "intercept_", 
      "description": "Constants in decision function.   See also -------- LinearSVC Implementation of Support Vector Machine classifier using the same library as this class (liblinear).  SVR Implementation of Support Vector Machine regression using libsvm: the kernel can be non-linear but its SMO algorithm does not scale to large number of samples as LinearSVC does.  sklearn.linear_model.SGDRegressor SGDRegressor can optimize the same cost function as LinearSVR by adjusting the penalty and loss parameters. In addition it requires less memory, allows incremental (online) learning, and implements various loss functions and regularization regimes."
    }
  ]
}
